package analysis

import (
	"errors"
	"fmt"
	"os"
)

// handles errors
func e(err error) {
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

// returns an interval by num
func getInterval(a int) int {
	num := nums[0]
	if a > nums[int(N-1)]+K {
		e(errors.New("a < num+K"))
	}
	for i := 1; ; i++ {
		if a >= num && a < num+K {
			return i
		}
		num += K
	}
}
