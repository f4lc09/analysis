package analysis

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"sort"
	"strconv"

	"image/color"

	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/vg"
)

var nums []int
var N float64
var K int
var answer map[string]string
var intervals []int
var frequency map[int]int
var cumulativeFrequency map[int]int
var relativeFrequency map[int]float64
var cumulativeRelativeFrequency map[int]float64
var mids map[int]float64

func LoadData(ns []int) {
	nums = ns
	calculate()
}

func Mod() float64 {
	res, _ := strconv.ParseFloat(answer["Мода"], 64)
	return res
}

func Average() float64 {
	res, _ := strconv.ParseFloat(answer["Среднее арифметическое"], 64)
	return res
}

func Median() float64 {
	res, _ := strconv.ParseFloat(answer["Медиана"], 64)
	return res
}

func ScopeVariation() float64 {
	res, _ := strconv.ParseFloat(answer["Размах вариации"], 64)
	return res
}

func Dispersion() float64 {
	res, _ := strconv.ParseFloat(answer["Дисперсия"], 64)
	return res
}

func StandardDeviation() float64 {
	res, _ := strconv.ParseFloat(answer["Среднее квадратичное отклонение"], 64)
	return res
}

func CoefficientOfVariation() float64 {
	res, _ := strconv.ParseFloat(answer["Коэффициент вариации"], 64)
	return res
}

func StandardErrorOfTheArithmeticMean() float64 {
	res, _ := strconv.ParseFloat(answer["Стандартная ошибка среднего арифметического"], 64)
	return res
}

func BevelMeasure() float64 {
	res, _ := strconv.ParseFloat(answer["Мера скошенности"], 64)
	return res
}

func Excess() float64 {
	res, _ := strconv.ParseFloat(answer["Эксцесс"], 64)
	return res
}

func calculate() {
	sort.Ints(nums)

	relativeFrequency = make(map[int]float64)
	cumulativeRelativeFrequency = make(map[int]float64)
	frequency = make(map[int]int)
	cumulativeFrequency = make(map[int]int)
	answer = make(map[string]string)
	mids = make(map[int]float64)

	N = float64(len(nums))
	if N == 0 {
		fmt.Println("input.json содержит пустой массив")
		return
	}

	intervalRange := nums[int(N-1)] - nums[0]
	intervalsCount := math.Floor(1 + math.Log2(N))
	K = int(math.Ceil(float64(intervalRange) / intervalsCount))
	for i := range intervalRange / K {
		i++
		intervals = append(intervals, i)
	}
	if intervalRange%K != 0 {
		intervals = append(intervals, intervals[len(intervals)-1]+1)
	}
	// Compute frequency, cumulative frequency, relative frequency, cumulative relative frequency, and midpoints
	for _, num := range nums {
		i := getInterval(num)
		frequency[i]++
	}

	for _, i := range intervals {
		v := frequency[i]
		cumulativeFrequency[i] = v + cumulativeFrequency[i-1]
	}

	for _, i := range intervals {
		v := frequency[i]
		relativeFrequency[i] = float64(v) / N
	}

	for _, i := range intervals {
		v := relativeFrequency[i]
		cumulativeRelativeFrequency[i] = v + cumulativeRelativeFrequency[i-1]
	}

	start := nums[0]
	for _, i := range intervals {
		mids[i] = float64((start*2 + K) / 2)
		start += K
	}

	// Statistics calculations

	// Average
	var sum float64
	for _, num := range nums {
		sum += float64(num)
	}
	sred := sum / N

	// Mode
	var modInterval int
	var max int
	for i, v := range frequency {
		if v > max {
			max = v
			modInterval = i
		}
	}
	modIntervalMaxValue := nums[0] + K*modInterval
	modIntervalMinValue := nums[0] + K*(modInterval-1)

	// intervalsCount := float64(len(intervals))
	chislitel := float64(frequency[modInterval] - frequency[modInterval-1])
	znamenatel := float64(frequency[modInterval] - frequency[modInterval-1] + frequency[modInterval] + frequency[modInterval+1])
	mod := float64(modIntervalMinValue) + (intervalsCount * chislitel / znamenatel)

	// Median
	chislitelf := intervalsCount * (0.5*N - float64(cumulativeFrequency[modInterval]))
	znamenatelf := float64(frequency[modInterval+1])
	med := float64(modIntervalMaxValue) + chislitelf/znamenatelf

	// Dispersion
	var iterDisSum float64
	for i, mid := range mids {
		iterDisSum += float64(frequency[i]) * math.Pow((mid-sred), 2)
	}
	dis := iterDisSum / (N - 1)

	// Mean square
	meanSquare := math.Sqrt(dis)

	// The coefficient of variation
	variCoef := meanSquare / sred * 100

	// Standard deviation of the arithmetic mean
	deviation := meanSquare / math.Sqrt(N)

	// Bevel measure
	bevelMeasure := sred - mod/meanSquare

	// Excess
	var iterExcSum float64
	for i, mid := range mids {
		iterExcSum += float64(frequency[i]) * math.Pow((mid-sred), 4)
	}
	znamenatelf = N * math.Pow(meanSquare, 4)
	exc := iterExcSum/znamenatelf - 3

	answer["Среднее арифметическое"] = fmt.Sprintf("%.1f", sred)
	answer["Мода"] = fmt.Sprintf("%.2f", mod)
	answer["Медиана"] = fmt.Sprintf("%.2f", med)
	answer["Размах вариации"] = strconv.Itoa(nums[len(nums)-1] - nums[0])
	answer["Дисперсия"] = fmt.Sprintf("%.2f", dis)
	answer["Среднее квадратичное отклонение"] = strconv.FormatFloat(meanSquare, 'f', 2, 64)
	answer["Коэффициент вариации"] = fmt.Sprintf("%2.2f", variCoef)
	answer["Стандартная ошибка среднего арифметического"] = fmt.Sprintf("%.2f", deviation)
	answer["Мера скошенности"] = fmt.Sprintf("%.2f", bevelMeasure)
	answer["Эксцесс"] = fmt.Sprintf("%.2f", exc)
}

func DrawTable() {
	fmt.Printf("%-4s | %-20s | %-8s | %-20s | %-11s | %-23s | %-16s\n",
		"i", "Границы интервала", "Частота", "Накопленная частота", "Частотность", "Накопленная частотность", "Среднее значение")
	fmt.Println("------------------------------------------------------------------------------------------------------------------------")
	for _, i := range intervals {
		intervalMin := nums[0] + K*(i-1)
		intervalMax := nums[0] + K*i
		fmt.Printf("%-4d | %-20s | %-8d | %-20d | %-11.2f | %-23.2f | %-16.2f\n",
			i, fmt.Sprintf("%d - %d", intervalMin, intervalMax), frequency[i], cumulativeFrequency[i], relativeFrequency[i], cumulativeRelativeFrequency[i], mids[i])
	}
}

func SaveHistogram() error {
	h := plot.New()

	h.Title.Text = "Гистограмма распределения"
	h.X.Label.Text = "Интервалы"
	h.Y.Label.Text = "Частота"

	bars := make(plotter.Values, len(intervals))
	for j, i := range intervals {
		bars[j] = float64(frequency[i])
	}

	barChart, err := plotter.NewBarChart(bars, vg.Points(20))
	if err != nil {
		return fmt.Errorf("could not save histogram: %w", err)
	}
	barChart.LineStyle.Width = vg.Length(0)
	barChart.Color = color.RGBA{0, 0, 255, 255}

	h.Add(barChart)
	err = h.Save(4*vg.Inch, 4*vg.Inch, "frequency_histogram.png")
	if err != nil {
		return fmt.Errorf("could not save histogram: %w", err)
	}

	fmt.Printf("\nПостроенная гистограмма записана в файл: frequency_histogram.png\n\n")

	fmt.Println("Нажмите Enter, чтобы закрыть программу...")
	_, err = bufio.NewReader(os.Stdin).ReadBytes('\n')
	if err != nil {
		return fmt.Errorf("could not save histogram: %w", err)
	}
	return nil
}

func SavePolygon() error {
	p := plot.New()

	p.Title.Text = "Полигон распределения частот"
	p.X.Label.Text = "Интервалы"
	p.Y.Label.Text = "Частота"

	points := make(plotter.XYs, len(intervals))
	for j, i := range intervals {
		points[j].X = mids[i]
		points[j].Y = float64(frequency[i])
	}

	line, err := plotter.NewLine(points)
	if err != nil {
		return fmt.Errorf("could not save polygon: %w", err)
	}
	line.LineStyle.Width = vg.Points(1)
	line.LineStyle.Color = plotter.DefaultLineStyle.Color

	p.Add(line)
	err = p.Save(4*vg.Inch, 4*vg.Inch, "frequency_polygon.png")
	if err != nil {
		return fmt.Errorf("could not save polygon: %w", err)
	}

	fmt.Printf("\nПостроенный полигон записан в файл: frequency_polygon.png\n\n")
	return nil
}
